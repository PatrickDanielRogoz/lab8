package rogoz.patrick.lab8.Ex1;

interface X {
    int metX();
}
public class Y {
}

class A implements X{
    public int metX(){
        return 0;
    }
}

class B extends A{
    X y;
    C c;
    B(X a){
        y=a;
        c=new C();
    }
    Y met2() {
        return new Y();
    }
}
class C{}
