package rogoz.patrick.lab8.Ex3;

import java.util.*;

public class Simulator {

    /**
     * @param args
     */
    public static void main(String[] args) {

        //build station Cluj-Napoca
        Controler[] c = new Controler[3];
        c[0]=new Controler("Satu Mare");
        c[1]=new Controler("CLuj");
        c[2]=new Controler("Bucuresti");

        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);

        c[0].addControlledSegment(s7);
        c[0].addControlledSegment(s8);
        c[0].addControlledSegment(s9);

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c[1].addControlledSegment(s1);
        c[1].addControlledSegment(s2);
        c[1].addControlledSegment(s3);




        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c[2].addControlledSegment(s4);
        c[2].addControlledSegment(s5);
        c[2].addControlledSegment(s6);

        Controler[] aux=new Controler[2];
        aux[0]=c[1];
        aux[1]=c[2];

        c[0].setNeighbourController(aux);
        aux[0]=c[0];
        aux[1]=c[2];
        c[1].setNeighbourController(aux);
        aux[0]=c[0];
        aux[1]=c[1];
        c[2].setNeighbourController(aux);

        //testing

        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca","R-002");
        s5.arriveTrain(t2);

        Train t3 = new Train("Satu Mare","D-007");
        s8.arriveTrain(t3);

        c[0].displayStationState();
        c[1].displayStationState();
        c[2].displayStationState();

        System.out.println("\nStart train control\n");

        //execute 3 times controller steps
        for(int i = 0;i<3;i++){
            System.out.println("### Step "+i+" ###");
            c[0].controlStep();
            c[1].controlStep();
            c[2].controlStep();

            System.out.println();

            c[0].displayStationState();
            c[1].displayStationState();
            c[2].displayStationState();

        }
    }

}

class Controler{

    String stationName;

    Controler[] neighbourController=new Controler[3];
    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();



    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler[] listV){
        for (int i=0;i<listV.length;i++)
            neighbourController[i]=listV[i];

    }

    void addControlledSegment(Segment s){
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId(){
        for(Segment s:list){
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep(){
        //check which train must be sent

        for(Segment segment:list){
            if(segment.hasTrain()){
                Train t = segment.getTrain();
                for (int i=0;i<neighbourController.length;i++) {
                    if (t.getDestination().equals(neighbourController[i].stationName)) {
                        //check if there is a free segment
                        int id = neighbourController[i].getFreeSegmentId();
                        if (id == -1) {
                            System.out.println("Trenul +" + t.name + "din gara " + stationName + " nu poate fi trimis catre " + neighbourController[i].stationName + ". Nici un segment disponibil!");
                            return;
                        }
                        //send train
                        System.out.println("Trenul " + t.name + " pleaca din gara " + stationName + " spre gara " + neighbourController[i].stationName);
                        segment.departTRain();
                        neighbourController[i].arriveTrain(t, id);
                    }
                }

            }
        }//.for

    }//.


    public void arriveTrain(Train t, int idSegment){
        for(Segment segment:list){
            //search id segment and add train on it
            if(segment.id == idSegment)
                if(segment.hasTrain()==true){
                    System.out.println("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
                    return;
                }else{
                    System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        System.out.println("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");

    }


    public void displayStationState(){
        System.out.println("=== STATION "+stationName+" ===");
        for(Segment s:list){
            if(s.hasTrain())
                System.out.println("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|");
            else
                System.out.println("|----------ID="+s.id+"__Train=______ catre ________----------|");
        }
    }
}


class Segment{
    int id;
    Train train;

    Segment(int id){
        this.id = id;
    }

    boolean hasTrain(){
        return train!=null;
    }

    Train departTRain(){
        Train tmp = train;
        this.train = null;
        return tmp;
    }

    void arriveTrain(Train t){
        train = t;
    }

    Train getTrain(){
        return train;
    }
}

class Train{
    String destination;
    String name;

    public Train(String destinatie, String nume) {
        super();
        this.destination = destinatie;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }

}

