package rogoz.patrick.lab8.Ex4;

import java.io.*;
import java.util.Random;

public class HomeAutomation {

    public static void main(String[] args) throws IOException {

        //test using an annonimous inner class

        Home h = new Home(){
            protected void setValueInEnvironment(Event event){
                System.out.println("New event in environment "+event);
                try {
                    FileWriter myWriter = new FileWriter("file.txt");
                    myWriter.write("New event in environment "+event);
                    myWriter.close();
                } catch (IOException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }
            }
            protected void controllStep() throws FileNotFoundException {
                System.out.println("Control step executed");
                try {
                    FileWriter myWriter = new FileWriter("file.txt");
                    myWriter.write("Control step executed");
                    myWriter.close();

                } catch (IOException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }
            }

        };
        h.simulate();
    }
}

abstract class Home {
    private Random r = new Random();
    private final int SIMULATION_STEPS = 20;

    protected abstract void setValueInEnvironment(Event event);
    protected abstract void controllStep() throws FileNotFoundException;

    private Event getHomeEvent(){
        //randomly generate a new event;
        int k = r.nextInt(100);
        if(k<30)
            return new NoEvent();
        else if(k<60)
            return new FireEvent(r.nextBoolean());
        else
            return new TemperatureEvent(r.nextInt(50));
    }

    public void simulate() throws FileNotFoundException {
        int k = 0;
        while(k <SIMULATION_STEPS){
            Event event = this.getHomeEvent();
            setValueInEnvironment(event);
            controllStep();

            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            k++;
        }
    }

}

abstract class Event {

    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    EventType getType() {
        return type;
    }

}

class TemperatureEvent extends Event {

    private int vlaue;
    private int auxTemp=20;

    TemperatureEvent(int vlaue) {
        super(EventType.FIRE.TEMPERATURE);
        this.vlaue = vlaue;
    }

    int getVlaue() {
        return vlaue;
    }

    @Override
    public String toString() {
        String s;
        if (vlaue<auxTemp)
            s="TemperatureEvent: Temperature("+vlaue+") Starting heating unit... ";
        else s="TemperatureEvent: Temperature("+vlaue+") Starting cooling unit...";
        return s;
    }

}

class FireEvent extends Event {

    private boolean smoke;

    FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;
    }

    boolean isSmoke() {
        return smoke;
    }

    @Override
    public String toString() {
        String s;
        if (smoke)
            s="FireEvent: Alarm is triggered! Calling the owner...";
        else s="FireEvent: No smoke";

        return s;
    }

}

class NoEvent extends Event{

    NoEvent() {
        super(EventType.NONE);
    }

    @Override
    public String toString() {
        return "NoEvent{}";
    }
}

enum EventType {
    TEMPERATURE, FIRE, NONE;
}

