package rogoz.patrick.lab8.Ex2;

interface A{
    void met1();
}

public class X {
    int p;
}

class Z{
    int k;
    public void met2(){
        System.out.println("met2");
    }
}

class Y extends Z implements  A{

    public void met1(){
        System.out.println("met1");
    }
    public void met3(){
        System.out.println("met3");
    }
}

class Q{
    Y r;
    Q(){
        r=new Y();
    }
}